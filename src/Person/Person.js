import React from "react";
const person = props => {
  return (
    <div>
      <h3 onClick={props.click}>
        I am {props.name} and i am {props.age} year old
      </h3>
      <p>{props.children}</p>
    </div>
  );
};

export default person;
