import React, { Component } from "react";
import "./App.css";
import Person from "./Person/Person";

class App extends Component {
  state = {
    persons: [
      { name: "Parth", age: 30 },
      { name: "Roy", age: 32 },
      { name: "Jason", age: 25 }
    ]
  };

  switchNameHandler = newName => {
    // this.state.persons[0].name = "Heee";
    this.setState({
      persons: [
        {
          name: newName,
          age: 26
        },
        { name: "Roy White", age: 36 },
        { name: "Jason Smith", age: 22 }
      ]
    });
  };

  render() {
    return (
      <div className="App">
        <h1>Welcome to React</h1>
        <button
          onClick={() => {
            this.switchNameHandler("Ralph");
          }}
        >
          Switch Name
        </button>
        <Person
          name={this.state.persons[0].name}
          age={this.state.persons[0].age}
        />
        <Person
          name={this.state.persons[1].name}
          age={this.state.persons[1].age}
          click={this.switchNameHandler.bind(this, "Breet Lee")}
        >
          My Hobbies : Gaming
        </Person>
        <Person
          name={this.state.persons[2].name}
          age={this.state.persons[2].age}
        />
      </div>
      // React.createElement(
      //   "div",
      //   { className: "App" },
      //   React.createElement("h1", null, "React Create Element is working")
      // )
    );
  }
}

export default App;
